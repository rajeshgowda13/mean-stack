import { Component, OnInit } from '@angular/core';
// import { Post } from '../post.model';
import { NgForm } from '@angular/forms';
import { PostService } from '../post.service';
@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit {
  // enteredTitle = '';
  // enteredContent = '';
  // @Output() postCreated = new EventEmitter<Post>();

  // onAddPost(postInput: HTMLTextAreaElement) {
  //   // console.log(postInput);
  //   this.newPost = postInput.value;
  // }

  onAddPost(form: NgForm) {
    // console.log(postInput);
    // this.newPost = this.enteredValue;
    if (form.invalid) {
      return;
    }
    this.postService.addPosts(form.value.title, form.value.content);
    form.resetForm();
    // const post: Post = {
    // title: this.enteredTitle,
    // title: form.value.title,
    // content: form.value.content,
    // date: form.value.date
    // content: this.enteredContent
    // };
    // this.postCreated.emit(post);
  }
  constructor(
    public postService: PostService
  ) { }

  ngOnInit() {
  }

}
